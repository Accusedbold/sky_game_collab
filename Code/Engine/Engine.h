#ifndef Engine_H
#define Engine_H

// Forward declarations
class ObjectFactory;

class Engine
{
    // Broadcasts the messages to all listeners
    void BroadcastMessages();
    // Notice I made the constructor private - this prevents instantiation without
    // using the singleton patter
    Engine();
    Engine *instance = nullptr;
    
    public:
    static Engine& getInstance(); // returns the reference to the instance
    // Update all the systems until the game is no longer active
    void GameLoop();
    // Release all the systems in reverse order that they were added
    int Release();
    // Adds a message to the ques of messages
    void RelayMessage(/*Message Stuff Later*/);
    // Broadcasts the message to listeners WHEN called
    void ImmediateMessage(/*Message Stuff Later*/);
    // Adds a system to the game
    void AddSystem(System &&);
    // Initializes all the systems
    void Initialize();
    // Get the dt
    float GetTimeStep();
    // Factory for objects
    ObjectFactory mFactory;
#   if defined(DEBUG)
std::map<std::string, float> mSysTimes;
#   endif
    
    private:
    // mutex for the messages
    std::mutex mMessageMutex;
    // vector for the new messages
    //std::queue</*message stuff*/> mNewMessages;
    // vetor for the messages to be deleted
    //std::queue</*message stuff*/> mOldMessages;
    // Tracks all the systems the game uses
    std::vector<System> mSystems;
    // The last time the game was updated
    unsigned mLastTime;
    // Is the game running
    bool mGameActive;
    // Is the game minimized
    bool mGameMinimized;
    // time dilation
    float mTimeDilation;
    // current elapsed time between frames
    float mTimeStep;
}