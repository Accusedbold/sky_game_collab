#ifndef Precompiled_H
#define Precompiled_H

// standard C library stuff
#include <string>    /* string */
#include <vector>    /* vector */
#include <map>       /* map */
#include <mutex>     /* mutex */
#include <queue>     /* queue */
#include <ctime>     /* clock, clock_t */
#include <memory>    /* shared pointer */
#include <istream>   /* input stream */

// Object Factory Stuff
#include "Component.h"        /* Component to the objects */
#include "Object.h"           /* Objects for the game */
#include "ObjectFactory.h"    /* Factory for the objects */
// Engine stuff
#include "Engine.h"       /* Core of the game */

#endif