// We'll have a precompiled header that we can use that will decrease build times
#include "precompiled.h" // The precompiled header will contain things that are used widely, but changed seldomly


// First you need the entry to the process, this is by default main on almost every compiler


void AddSystems()
{
    // it should be as simple as taking our Engine object and adding a system object.
    Engine engineObject = Engine.getInstance(); // This is a singelton, I'm not used to using them, but we will for this game, thanks
    //engineObject.AddSystem(GraphicsSystem.getInstance()); // it should work like this
}

int main(int argc, char** argv)
{
    // Game Logic
    // We will add systems to our engine in a modula way so growing the game will be simple
    AddSystems();
    // Next we will initialize all the systems in the order they were added
    Engine engObj = Engine.getInstance();
    engObj.Initialize();
    engObj.GameLoop();
        
    return 0;
}